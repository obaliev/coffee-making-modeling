# README #

It's simple modeling of coffee making system for coffee lovers.

In example, we model system's work for 1000 programmers who:

1. Reserve some coffee
1. Pay for it
1. Look for coffee machine, make it and go away with a coffee.

To try it you need only java 8 and execute: *mvn clean package assembly:single && java -jar target/goodgames-coffee-system-modeling-1.0-SNAPSHOT-jar-with-dependencies.jar 100*

Last parameter can be used for modeling for specific number of programmers. (Default is 100)

An example of result (for 1000):


```
#!java

Paid coffee: 1000
Paid by cash: 456
Paid by credit: 544

CoffeeMachine{id=1}:
Made coffee: 500
Made espresso: 165
Made latte: 184
Made cappuccino: 151

CoffeeMachine{id=2}:
Made coffee: 500
Made espresso: 167
Made latte: 159
Made cappuccino: 174


Avg programmers time spent: 1498.25
Lowest programmer spent: 1250ms
Fastest programmer spent: 1750ms

```