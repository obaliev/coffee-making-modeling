package org.balev.goodgames.test.model;

import org.junit.Test;

/**
 * Created by obaliev on 12/12/15.
 */
public class CoffeeMachinesHolderTest {

    @Test
    public void testGetNextCoffeeMachine() throws Exception {
        // given
        CoffeeMachinesHolder coffeeMachinesHolder = new CoffeeMachinesHolder(2);

        // when
        for (int i = 0; i < 20; i++) {
            coffeeMachinesHolder.getNextCoffeeMachine();
        }

        // then no errors
    }
}