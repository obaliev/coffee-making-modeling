package org.balev.goodgames.test.util;

import org.hamcrest.Matchers;
import org.junit.Test;

import static org.junit.Assert.assertThat;

/**
 * Created by obaliev on 12/12/15.
 */
public class ModelingUtilsTest {

    public static final int NORMAL_DEVIATION = 10;

    @Test
    public void testWaitFor() throws Exception {
        // given
        final float seconds = 0.5f;

        // when
        long startTime = System.currentTimeMillis();
        ModelingUtils.waitFor(seconds);
        long endTime = System.currentTimeMillis();

        // then
        assertThat(endTime - startTime, Matchers.lessThanOrEqualTo((long) (seconds * 1000 + NORMAL_DEVIATION)));

    }
}