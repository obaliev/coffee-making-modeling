package org.balev.goodgames.test;

import org.balev.goodgames.test.model.CoffeeType;
import org.balev.goodgames.test.model.PaymentType;
import org.junit.Test;

/**
 * Created by obaliev on 12/12/15.
 */
public class ModelingSytemTest {

    @Test
    public void testRandomCoffeeType() throws Exception {
        // when
        for (int i = 0; i < CoffeeType.values().length * 10; i++) {
            ModelingSytem.randomCoffeeType();
        }

        // then now errors;
    }

    @Test
    public void testRandomPaymentType() throws Exception {
        // when
        for (int i = 0; i < 20; i++) {
            PaymentType paymentType = ModelingSytem.randomPaymentType();
        }

        // then now errors;
    }
}