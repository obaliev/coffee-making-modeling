package org.balev.goodgames.test;

import org.balev.goodgames.test.model.*;
import org.balev.goodgames.test.util.MeasureUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * Main class for running coffee making modeling system
 * <p>
 * Created by obaliev on 12/12/15.
 */
public class ModelingSytem {

    public static int DEFAULT_NUMBER_OF_PROGRAMMERS = 100;

    public static void main(String[] args) {
        int readNumberOfProgrammers = readNumberOfProgrammersFromArgs(args);
        final int numberOfProgrammers = readNumberOfProgrammers == 0 ? DEFAULT_NUMBER_OF_PROGRAMMERS : readNumberOfProgrammers;

        ExecutorService pickFavouriteCoffeeExecutorService = Executors.newFixedThreadPool(10);
        ExecutorService payForCoffeeExecutorService = Executors.newFixedThreadPool(5);
        ExecutorService makingCoffeeExecutorService = Executors.newFixedThreadPool(2);
        CoffeeMachinesHolder coffeeMachinesHolder = new CoffeeMachinesHolder(2);
        CoffeeSystem coffeeSystem = new CoffeeSystem();
        List<CompletableFuture> waitForMakingCoffeeCompletableFutures = new ArrayList<>();

        System.out.println(String.format("Modeling system for %d programmers", numberOfProgrammers));

        for (int i = 0; i < numberOfProgrammers; i++) {
            Programmer programmer = new Programmer(randomCoffeeType(), randomPaymentType());
            CompletableFuture completableFuture = CompletableFuture.runAsync(() -> programmer.pickFavouriteCoffee(coffeeSystem), pickFavouriteCoffeeExecutorService)
                    .thenRunAsync(() -> programmer.payForCoffee(coffeeSystem), payForCoffeeExecutorService)
                    .thenRunAsync(() -> programmer.makeACoffee(coffeeMachinesHolder.getNextCoffeeMachine()), makingCoffeeExecutorService);
            waitForMakingCoffeeCompletableFutures.add(completableFuture);
        }

        CompletableFuture[] completableFutures = waitForMakingCoffeeCompletableFutures.toArray(new CompletableFuture[waitForMakingCoffeeCompletableFutures.size()]);

        System.out.print("Just waiting");
        while (!CompletableFuture.allOf(completableFutures).isDone()) {
            try {
                System.out.print('.');
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                throw new IllegalStateException("Abnormal termination.", e);
            }
        }
        System.out.println("Done\n");

        pickFavouriteCoffeeExecutorService.shutdown();
        payForCoffeeExecutorService.shutdown();
        makingCoffeeExecutorService.shutdown();

        // show gathered statistics
        System.out.println("Paid coffee: " + (MeasureUtils.PAID_COFFEE_BY_CASH.get() + MeasureUtils.PAID_COFFEE_BY_CREDIT.get()));
        System.out.println("Paid by cash: " + MeasureUtils.PAID_COFFEE_BY_CASH.get());
        System.out.println("Paid by credit: " + MeasureUtils.PAID_COFFEE_BY_CREDIT.get());
        System.out.println();
        for (Map.Entry<CoffeeMachine, MeasureUtils.CoffeeMade> entry : MeasureUtils.COFFEE_MACHINE_COFFEE_MADE.entrySet()) {
            System.out.println(entry.getKey() + ":");
            System.out.println("Made coffee: " + (entry.getValue().madeEspresso.get() + entry.getValue().madeLatteMacchiato.get() + entry.getValue().madeCappuccino.get()));
            System.out.println("Made espresso: " + entry.getValue().madeEspresso.get());
            System.out.println("Made latte: " + entry.getValue().madeLatteMacchiato.get());
            System.out.println("Made cappuccino: " + entry.getValue().madeCappuccino.get());
            System.out.println();
        }

        System.out.println();
        List<Integer> programmersTimeSpent = MeasureUtils.PROGRAMMER_COFFEE_MADE_TIME_SPEND.entrySet().stream()
                .map(entry -> entry.getValue().get())
                .sorted(Integer::compareTo).collect(Collectors.toList());
        System.out.println("Avg programmers time spent: " + programmersTimeSpent.stream().mapToInt(i -> i).average().getAsDouble());
        System.out.println("Lowest programmer spent: " + programmersTimeSpent.get(0) + "ms");
        System.out.println("Fastest programmer spent: " + programmersTimeSpent.get(programmersTimeSpent.size() - 1) + "ms");

    }

    private static int readNumberOfProgrammersFromArgs(String[] args) {
        int arg = 0;

        if (args.length == 1) {
            try {
                arg = Integer.valueOf(args[0]);
            } catch (NumberFormatException e) {
                System.out.println("Number of Programmers should be only integer!");
            }

            if (arg <= 0) {
                System.out.println("Number of Programmers cannot be <= 0");
            }
        } else {
            System.out.println("You can define 1 param - number of Programmers who will pay and make a coffee!");
        }

        return arg;
    }

    public static CoffeeType randomCoffeeType() {
        return CoffeeType.values()[new Random().nextInt(CoffeeType.values().length)];
    }

    public static PaymentType randomPaymentType() {
        return PaymentType.values()[new Random().nextInt(PaymentType.values().length)];
    }
}
