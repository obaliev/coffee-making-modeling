package org.balev.goodgames.test.util;

import java.util.concurrent.TimeUnit;

/**
 * Created by obaliev on 12/12/15.
 */
public class ModelingUtils {

    /**
     * Thread will sleep
     *
     * @param seconds seconds to sleep (i.e. <b>0.5f</b> - means half of a second).
     */
    public static void waitFor(float seconds) {
        try {
            long millisec = (long) (seconds * 1000);
            TimeUnit.MILLISECONDS.sleep(millisec);
        } catch (InterruptedException e) {
            throw new IllegalStateException(String.format("Unable to wait for %f seconds", seconds), e);
        }
    }
}
