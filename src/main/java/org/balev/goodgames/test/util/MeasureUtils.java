package org.balev.goodgames.test.util;

import org.balev.goodgames.test.model.CoffeeMachine;
import org.balev.goodgames.test.model.CoffeeType;
import org.balev.goodgames.test.model.PaymentType;
import org.balev.goodgames.test.model.Programmer;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by obaliev on 12/12/15.
 */
public class MeasureUtils {

    public final static AtomicInteger PAID_COFFEE_BY_CASH = new AtomicInteger(0);
    public final static AtomicInteger PAID_COFFEE_BY_CREDIT = new AtomicInteger(0);

    /**
     * Here we keep info about how much coffee each coffee machine made
     */
    public final static Map<CoffeeMachine, CoffeeMade> COFFEE_MACHINE_COFFEE_MADE = new ConcurrentHashMap<>();

    /**
     * Here we keep the time in millisec which programmer spend on making coffee
     */
    public final static Map<Programmer, AtomicInteger> PROGRAMMER_COFFEE_MADE_TIME_SPEND = new ConcurrentHashMap<>();

    public static void waitForAndLogSpentTime(float seconds, Programmer programmer) {
        ModelingUtils.waitFor(seconds);
        logTheTime(seconds, programmer);
    }

    public static void logTheTime(float seconds, Programmer programmer) {
        MeasureUtils.countSpentTime(programmer, (int) (seconds * 1000));
    }

    public static void countPaidCoffee(PaymentType paymentType) {
        switch (paymentType) {
            case CASH:
                PAID_COFFEE_BY_CASH.incrementAndGet();
                break;
            case CREDIT:
                PAID_COFFEE_BY_CREDIT.incrementAndGet();
                break;
        }
    }

    public static void countSpentTime(Programmer programmer, float seconds) {
        countSpentTime(programmer, (int) (seconds * 1000));
    }

    public static void countSpentTime(Programmer programmer, int millisec) {
        AtomicInteger timeSpent = PROGRAMMER_COFFEE_MADE_TIME_SPEND.get(programmer);

        if (timeSpent == null) {
            timeSpent = new AtomicInteger(0);
        }

        timeSpent.addAndGet(millisec);
        PROGRAMMER_COFFEE_MADE_TIME_SPEND.put(programmer, timeSpent);
    }

    public static void countMadeCoffee(CoffeeMachine coffeeMachine, CoffeeType coffeeType) {
        CoffeeMade coffeeMade = COFFEE_MACHINE_COFFEE_MADE.get(coffeeMachine);

        if (coffeeMade == null) {
            coffeeMade = new CoffeeMade();
        }

        coffeeMade.countMadeCoffee(coffeeType);

        COFFEE_MACHINE_COFFEE_MADE.put(coffeeMachine, coffeeMade);
    }

    public static class CoffeeMade {
        /**
         * It's not very neccessary to have atomic integer here, as we're synchronizing by coffee machine and COFFEE_MACHINE_COFFEE_MADE already. <br />
         * In this case it's just for potential separate using of this class outside our synchronization.
         */
        public final AtomicInteger madeEspresso = new AtomicInteger(0);
        public final AtomicInteger madeLatteMacchiato = new AtomicInteger(0);
        public final AtomicInteger madeCappuccino = new AtomicInteger(0);

        public void countMadeCoffee(CoffeeType coffeeType) {
            switch (coffeeType) {
                case ESPRESSO:
                    this.madeEspresso.incrementAndGet();
                    break;
                case LATTE_MACCHIATO:
                    this.madeLatteMacchiato.incrementAndGet();
                    break;
                case CAPPUCCINO:
                    this.madeCappuccino.incrementAndGet();
                    break;
            }
        }
    }
}
