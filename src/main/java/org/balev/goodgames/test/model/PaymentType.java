package org.balev.goodgames.test.model;

/**
 * Created by obaliev on 12/12/15.
 */
public enum PaymentType {
    CASH, CREDIT
}
