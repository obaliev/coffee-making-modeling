package org.balev.goodgames.test.model;

import org.balev.goodgames.test.util.MeasureUtils;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by obaliev on 12/12/15.
 */
public class Programmer {

    /**
     * It's just for easier logging between threads to understand what's happening.
     */
    private static AtomicInteger idGen = new AtomicInteger(0);
    private final int id = idGen.incrementAndGet();

    private CoffeeType coffeeType;
    private PaymentType paymentType;

    public Programmer(CoffeeType coffeeType, PaymentType paymentType) {
        this.coffeeType = coffeeType;
        this.paymentType = paymentType;
    }

    public void pickFavouriteCoffee(CoffeeSystem coffeeSystem) {
        coffeeSystem.pickFavouriteCoffee(this);
//        System.out.println(this + ": chose favourite type: " + coffeeType);
    }

    public void payForCoffee(CoffeeSystem coffeeSystem) {
        coffeeSystem.payForCoffee(this);
//        System.out.println(this + ": paid for coffee: " + coffeeType);
    }

    public synchronized void makeACoffee(CoffeeMachine coffeeMachine) {
        // We have to capture Coffee Machine during making a coffee.
        synchronized (coffeeMachine) {
            findACup();
            putTheCupUnderTheOutlet(coffeeMachine);
            pickCoffee(coffeeMachine);
            leaveCoffeeMachine();
//            System.out.println(this + ": took coffee and left: " + coffeeType);
        }
    }

    protected void findACup() {
        MeasureUtils.waitForAndLogSpentTime(0.25f, this);
    }

    protected void putTheCupUnderTheOutlet(CoffeeMachine coffeeMachine) {
        MeasureUtils.waitForAndLogSpentTime(0.25f, this);
    }

    protected void pickCoffee(CoffeeMachine coffeeMachine) {
        MeasureUtils.waitForAndLogSpentTime(0.25f, this);

        coffeeMachine.selectCoffeeTypeAndMakeIt(coffeeType);

        MeasureUtils.countSpentTime(this, coffeeMachine.getMakeCoffeeTime(coffeeType));
    }

    protected void leaveCoffeeMachine() {
        MeasureUtils.waitForAndLogSpentTime(0.25f, this);
    }

    public PaymentType getPaymentType() {
        return paymentType;
    }

    @Override
    public String toString() {
        return "Programmer{id=" + id + '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Programmer that = (Programmer) o;

        return id == that.id;

    }

    @Override
    public int hashCode() {
        return id;
    }
}
