package org.balev.goodgames.test.model;

/**
 * Created by obaliev on 12/12/15.
 */
public enum CoffeeType {
    ESPRESSO, LATTE_MACCHIATO, CAPPUCCINO
}
