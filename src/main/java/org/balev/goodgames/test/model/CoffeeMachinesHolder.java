package org.balev.goodgames.test.model;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Here we just keep some coffee machines
 * <p>
 * Created by obaliev on 12/12/15.
 */
public class CoffeeMachinesHolder {

    private List<CoffeeMachine> coffeeMachines = new ArrayList<>();
    private AtomicInteger currentCoffeeMachineIndex = new AtomicInteger(0);

    public CoffeeMachinesHolder(int coffeeMachinesCount) {
        if (coffeeMachinesCount <= 0) {
            throw new IllegalArgumentException("Illeagal coffee machines count: " + coffeeMachinesCount);
        }

        for (int i = 0; i < coffeeMachinesCount; i++) {
            coffeeMachines.add(new CoffeeMachine());
        }
    }

    /**
     * Using for getting next coffee machine for programmer who wants to make a coffee on it
     *
     * @return next available coffee machine
     */
    public CoffeeMachine getNextCoffeeMachine() {
        return coffeeMachines.get(currentCoffeeMachineIndex.incrementAndGet() % coffeeMachines.size());
    }
}
