package org.balev.goodgames.test.model;

import org.balev.goodgames.test.util.MeasureUtils;
import org.balev.goodgames.test.util.ModelingUtils;

/**
 * Represents some information system for choosing and paying coffee
 * <p>
 * Created by obaliev on 12/12/15.
 */
public class CoffeeSystem {

    public void pickFavouriteCoffee(Programmer programmer) {
        ModelingUtils.waitFor(0.5f);
    }

    public void payForCoffee(Programmer programmer) {
        switch (programmer.getPaymentType()) {
            case CASH:
                ModelingUtils.waitFor(0.5f);
                break;
            case CREDIT:
                ModelingUtils.waitFor(0.25f);
                break;
            default:
                throw new IllegalStateException("Unknown payment type: " + programmer.getPaymentType());
        }
        MeasureUtils.countPaidCoffee(programmer.getPaymentType());
    }
}
