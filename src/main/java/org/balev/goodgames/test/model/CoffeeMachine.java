package org.balev.goodgames.test.model;

import org.balev.goodgames.test.util.MeasureUtils;
import org.balev.goodgames.test.util.ModelingUtils;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by obaliev on 12/12/15.
 */
public class CoffeeMachine {

    /**
     * It's just for easier logging between threads to understand what's happening.
     */
    private static AtomicInteger idGen = new AtomicInteger(0);
    private final int id = idGen.incrementAndGet();

    private CoffeeType selectedCoffeeType;

    public void selectCoffeeTypeAndMakeIt(CoffeeType coffeeType) {
        this.selectedCoffeeType = coffeeType;

        makeCoffee();
    }

    private void makeCoffee() {
        if (selectedCoffeeType == null) {
            throw new IllegalStateException("Please select coffee type before making!");
        }

        ModelingUtils.waitFor(getMakeCoffeeTime(selectedCoffeeType));

        MeasureUtils.countMadeCoffee(this, selectedCoffeeType);

        resetSelectedCoffeeType();
    }

    public float getMakeCoffeeTime(CoffeeType coffeeType) {
        switch (coffeeType) {
            case ESPRESSO:
                return 0.25f;
            case LATTE_MACCHIATO:
                return 0.5f;
            case CAPPUCCINO:
                return 0.75f;
            default:
                throw new IllegalStateException("Cannot find estimated time for selected coffee type: " + coffeeType);
        }
    }

    private void resetSelectedCoffeeType() {
        this.selectedCoffeeType = null;
    }

    @Override
    public String toString() {
        return "CoffeeMachine{id=" + id + '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CoffeeMachine that = (CoffeeMachine) o;

        return id == that.id;
    }

    @Override
    public int hashCode() {
        return id;
    }
}
